<?php
/**
 * Post rendering content according to caller of get_template_part.
 *
 * @package Understrap
 * @subpackage Democrats\Loop_Templates
 * @since 0.0.1
 */

?>

<article <?php post_class( 'col-12' ); ?> id="post-<?php the_ID(); ?>">
	<?php get_template_part( 'global-templates/card' ); ?>
</article><!-- #post-## -->
