<?php
/**
 * Single post partial template.
 *
 * @package Understrap
 * @subpackage Democrats\Loop_Templates
 * @since 0.0.1
 */

?>
<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">
	<?php get_template_part( 'global-templates/card' ); ?>
</article><!-- #post-## -->
