# Democrats.com Theme

Child theme of [understrap](https://understrap.com/) for the Democrats.com website.

## Developing Instructions

Clone this repo into a working Wordpress install under `wp-content/themes/`.
Make sure you have the understrap (parent) theme installed in the `themes` directory.

Run NPM install:

`npm install`

Then run the gulp process with:

`gulp`

Prepare for production:

`gulp scss-for-prod`

Prepare for distribution:

`gulp dist`
