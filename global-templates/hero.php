<?php
/**
 * Hero setup.
 *
 * @package Understrap
 * @subpackage Democrats\Global_Templates
 * @since 0.0.1
 */

?>

<div class="wrapper" id="wrapper-hero">

	<?php get_template_part( 'loop-templates/content' ); ?>

</div>
