<?php
/**
 * Card setup.
 *
 * @package Understrap
 * @subpackage Democrats\Global_Templates
 * @since 0.0.1
 */

$card_class = 'card' ;
$card_class = ( is_single( $post->ID ) ) ? $card_class . ' card-singular' : $card_class ;

// TODO test for thumbnail and set default
$featured_img = get_the_post_thumbnail_url( $post->ID, 'medium' );
?>

<div class="<?php echo $card_class ?>" style="background-image: url(<?php echo $featured_img; ?>)">

  <header class="card-block entry-header">
    <?php
    if ( is_single( $post->ID ) ) {
      the_title( '<h1 class="entry-title">', '</h1>' );
    } else {
      the_title( sprintf( '<h4 class="card-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ),
      '</a></h4>' );
    }
    ?>
    <?php /* Date format can be changed in Settings > General */ ?>
    <div class="entry-meta"><?php the_date( get_option( 'date_format' ) ); ?></div>
  </header>


  <?php if ( is_single( $post->ID ) ): ?>
  <section class="card-block card-social entry-meta">
    <a href="#" class="card-link card-link-social"><i class="fa fa-2x fa-twitter" aria-hidden="true"></i></a>
    <a href="#" class="card-link card-link-social"><i class="fa fa-2x fa-facebook-square" aria-hidden="true"></i></a>
  </section>

  <section class="card-block card-entry entry-content">
    <?php
    echo get_the_post_thumbnail( $post->ID, 'large' );
    the_content();
    ?>
  </section>
  <?php endif; ?>

  <footer class="entry-footer">
    <?php if ( ! is_single( $post->ID ) ): ?>
      <a href="<?php echo esc_url( get_permalink() ); ?>" class="card-link btn btn-primary btn-sm"><?php _e( 'Read More', 'democrats' ); ?></a>
    <?php endif; ?>

      <a href="#" class="card-link card-link-social"><i class="fa fa-2x fa-twitter" aria-hidden="true"></i></a>
      <a href="#" class="card-link card-link-social"><i class="fa fa-2x fa-facebook-square" aria-hidden="true"></i></a>

  </footer>

</div>
